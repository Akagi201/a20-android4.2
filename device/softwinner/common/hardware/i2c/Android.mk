LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= insmod_device.c 
LOCAL_MODULE := insmod_device

LOCAL_SHARED_LIBRARIES:= libcutils libnetutils liblog 
    
LOCAL_MODULE_TAGS := eng optional
LOCAL_PRELINK_MODULE := false

include $(BUILD_EXECUTABLE)